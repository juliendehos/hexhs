-- Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
-- This work is free. You can redistribute it and/or modify it under the
-- terms of the Do What The Fuck You Want To Public License, Version 2,
-- as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

import HexhsJeu
import HexhsPlateau
import Graphics.UI.Gtk hiding (rectangle)
import Graphics.Rendering.Cairo 
import Data.IORef
import Data.Maybe
import Control.Monad

main :: IO ()
main = do
  initGUI
  jeuRef <- newIORef newJeu
  let rayon = 24
      marque = 6
      taille = plateauTaille
      taille' = fromIntegral taille
      largeur = round $ rayon*2*0.866*(1.5*(taille'+1))
      hauteur = round $ rayon*1.5*(taille'+3)
      
  -- window
  window <- windowNew
  widgetSetSizeRequest window largeur hauteur
  set window [ windowTitle := "HEX", windowResizable := False ]
  -- drawingArea
  canvas <- drawingAreaNew
  containerAdd window canvas
  widgetShowAll window
  -- expose event
  drawWindow <- widgetGetDrawWindow canvas
  canvas `on` exposeEvent $ tryEvent $ liftIO $
         gererExposeEvent drawWindow jeuRef largeur hauteur taille rayon marque
  -- double click event
  canvas `on` buttonPressEvent $ tryEvent $ do
    DoubleClick <- eventClick
    liftIO $ return ()
  -- left button event
  canvas `on` buttonPressEvent $ tryEvent $ do
    LeftButton <- eventButton
    (x,y) <- eventCoordinates
    liftIO $ gererButtonPressEvent drawWindow jeuRef largeur hauteur x y rayon
  -- destroy event      
  onDestroy window mainQuit
  mainGUI

gererExposeEvent :: DrawWindow -> IORef Jeu -> Int -> Int -> Int -> Double -> Double -> IO ()
gererExposeEvent drawWindow jeuRef largeur hauteur taille rayon marque = do
    jeu <- readIORef jeuRef
    renderWithDrawable drawWindow $ dessinerPlateau largeur hauteur taille rayon marque jeu

gererButtonPressEvent :: DrawWindow -> IORef Jeu -> Int -> Int -> Double -> Double -> Double -> IO ()
gererButtonPressEvent drawWindow jeuRef largeur hauteur x y r = do
    let rx = r * 0.866
        i = floor $ (0.667*y/r) - 0.5
        j = floor $ 1 + (0.5*x/rx) - (0.333*y/r)
    liftIO $ do 
         jeu <- readIORef jeuRef
         let jeu' = jeuClicCase jeu (i, j)
             vainqueurM = jeuGetVainqueurM jeu'
         writeIORef jeuRef jeu'
         actualiser drawWindow largeur hauteur
         unless (isNothing vainqueurM) $ do
            afficherMessage $ "Vainqueur : " ++ (show.fromJust) vainqueurM
            writeIORef jeuRef newJeu
            actualiser drawWindow largeur hauteur

actualiser :: DrawWindow -> Int -> Int -> IO ()
actualiser win l h = drawWindowInvalidateRect win (Rectangle 0 0 l h) False  

afficherMessage :: String -> IO ()
afficherMessage s = do
  dialog <- messageDialogNew Nothing [] MessageInfo ButtonsOk s
  dialogRun dialog
  widgetDestroy dialog
    
dessinerPlateau :: Int -> Int -> Int -> Double -> Double -> Jeu -> Render()
dessinerPlateau largeur hauteur taille rayon marque jeu = do
  -- dessine le fond
  setSourceRGB 0.8 0.8 0.8
  rectangle 0 0  (fromIntegral largeur)  (fromIntegral hauteur)
  fill
  stroke
  -- dessine les pions
  setSourceRGB 1 1 0
  mapM_ dessinerPlein jaunes
  setSourceRGB 0 1 1
  mapM_ dessinerPlein cyans
  -- dessine la marque du dernier pion
  setSourceRGB 1 0 0
  dessinerPoint rayon derniere marque
  -- dessine les contours
  setSourceRGB 0 0 0
  mapM_ dessinerVide jaunes
  mapM_ dessinerVide cyans
  mapM_ dessinerVide libres
  where dessinerVide = dessinerHexagoneVide rayon
        dessinerPlein = dessinerHexagonePlein rayon
        derniere = jeuGetDerniereCase jeu
        (cyans, jaunes, libres) = jeuGetCyansJaunesLibres jeu

ijtoxy :: Double -> (Int, Int) -> (Double, Double)
ijtoxy r (i, j) = (x, y)
  where i' = fromIntegral i
        j' = fromIntegral j
        x = r * 0.866 * (2 * j' + i')
        y = 1.5 * r * (i' + 1)

tracerHexagone :: Double -> Case -> Render()
tracerHexagone r c = do
  moveTo x (y-r)
  lineTo (x-rx) (y-ry)
  lineTo (x-rx) (y+ry)
  lineTo x (y+r)
  lineTo (x+rx) (y+ry)
  lineTo (x+rx) (y-ry)
  lineTo x (y-r)
  where (x, y) = ijtoxy r c
        rx = r * 0.866
        ry = r * 0.5

dessinerHexagonePlein :: Double -> Case -> Render()
dessinerHexagonePlein r p = do
  tracerHexagone r p
  fill
  stroke

dessinerHexagoneVide :: Double -> Case -> Render()
dessinerHexagoneVide r p = do
  tracerHexagone r p
  stroke

dessinerPoint :: Double -> Case -> Double -> Render()
dessinerPoint r c s = do
  save
  translate x y 
  scale s s
  arc 0 0 1 0 7
  fill
  restore
  stroke
  where (x, y) = ijtoxy r c
