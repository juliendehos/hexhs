-- Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
-- This work is free. You can redistribute it and/or modify it under the
-- terms of the Do What The Fuck You Want To Public License, Version 2,
-- as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

module HexhsJeu
( Jeu, newJeu, jeuGetVainqueurM, jeuClicCase, jeuGetDerniereCase, jeuGetCyansJaunesLibres
) where

import HexhsPlateau
import HexhsJoueur
import Data.Maybe

type Jeu = (Plateau, Joueur, Joueur)

newJeu :: Jeu
newJeu = let plateau = plateauNew 
         in (plateau, joueurNouveauCoup joueurNewHumain plateau, joueurNewIa)

jeuGetVainqueurM :: Jeu -> Maybe Couleur
jeuGetVainqueurM (p, _, _) = plateauGetVainqueurM p

jeuClicCase :: Jeu -> Case -> Jeu
jeuClicCase (plateau, joueurCyan, joueurJaune) caseCliquee = (plateau', joueurCyan', joueurJaune')
    where caseJouee =  joueurClicCase (if couleur == Cyan then joueurCyan else joueurJaune) caseCliquee
          plateau' = plateauJouerCase plateau caseJouee
          couleur = plateauGetCouleurCourante plateau
          joueurCyan' = if couleur == Cyan then joueurNouveauCoup joueurCyan plateau' else joueurCyan
          joueurJaune' = if couleur == Jaune then joueurNouveauCoup joueurJaune plateau' else joueurJaune

jeuGetDerniereCase :: Jeu -> Case
jeuGetDerniereCase (plateau, _, _) = plateauGetDerniereCase plateau

jeuGetCyansJaunesLibres :: Jeu -> ([Case], [Case], [Case])
jeuGetCyansJaunesLibres (p, _, _) = plateauGetCyansJaunesLibres p
