-- Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
-- This work is free. You can redistribute it and/or modify it under the
-- terms of the Do What The Fuck You Want To Public License, Version 2,
-- as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

module HexhsJoueur
( Joueur(..), joueurNewHumain, joueurNewIa, joueurNouveauCoup, joueurClicCase
) where

import HexhsPlateau
import Data.Maybe

data Joueur = Humain (Maybe Case) | Ia Case

joueurNewHumain :: Joueur
joueurNewHumain = Humain Nothing

joueurNewIa :: Joueur
joueurNewIa = Ia (1, 1)

joueurNouveauCoup :: Joueur -> Plateau -> Joueur
joueurNouveauCoup (Humain _) _ =  Humain Nothing
joueurNouveauCoup (Ia _) p = Ia (1, 1)
-- todo implementer une IA

joueurClicCase :: Joueur -> Case -> Case
-- todo : joueurClicCase (Ia c0) _ = c0
joueurClicCase (Ia _) c1 = c1
joueurClicCase (Humain _) c1 = c1

