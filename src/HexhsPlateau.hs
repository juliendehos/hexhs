-- Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
-- This work is free. You can redistribute it and/or modify it under the
-- terms of the Do What The Fuck You Want To Public License, Version 2,
-- as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

module HexhsPlateau
( Plateau, Couleur(..), Case, plateauTaille
, plateauNew, plateauGetDerniereCase, plateauGetCouleurCourante
, plateauJouerCase, plateauGetVainqueurM, plateauGetCyansJaunesLibres
) where

import Data.Maybe
import Data.List

data Couleur = Cyan | Jaune deriving (Eq, Show)
type Case = (Int, Int) 
type Composante = ([Case], Bool, Bool)
type DonneesJoueur = ([Case], [Case], [Composante])
type Plateau = (Maybe Couleur, Couleur, Case, DonneesJoueur, DonneesJoueur, [Case])

plateauTaille :: Int
plateauTaille = 13


plateauNew :: Plateau
plateauNew = (Nothing, Cyan, (0, 2), donneesCyan, donneesJaune, casesLibres)
    where bord1Jaune = [ (0,j) | j <- [2..plateauTaille] ]
          bord2Jaune = [ (plateauTaille+1,j) | j <- [1..plateauTaille-1] ]
          bord1Cyan = [ (i,0) | i <- [2..plateauTaille] ]
          bord2Cyan = [ (i,plateauTaille+1) | i <- [1..plateauTaille-1] ]
          donneesCyan = (bord1Cyan, bord2Cyan, [([], False, False)])
          donneesJaune = (bord1Jaune, bord2Jaune, [([], False, False)])
          casesLibres = [ (i,j) | i <- [1..plateauTaille], j <- [1..plateauTaille] ]

plateauGetDerniereCase :: Plateau -> Case
plateauGetDerniereCase (_, _, derniereCase, _, _, _) = derniereCase

plateauGetCouleurCourante :: Plateau -> Couleur
plateauGetCouleurCourante (_, couleurCourante, _, _, _, _) = couleurCourante

plateauJouerCase :: Plateau -> Case -> Plateau
plateauJouerCase p@(vainqueur, couleurCourante, derniereCase, donneesCyan, donneesJaunes, casesLibres) caseJouee = 
    if caseJouee `elem` casesLibres && isNothing vainqueur 
    then (vainqueur', couleurCourante', derniereCase', donneesCyan', donneesJaunes', casesLibres')
    else p
    where couleurCourante' = getCouleurSuivante couleurCourante
          derniereCase' = caseJouee
          donneesCyan' = if couleurCourante == Cyan then majDonnees donneesCyan caseJouee else donneesCyan
          donneesJaunes' = if couleurCourante == Jaune then majDonnees donneesJaunes caseJouee else donneesJaunes
          casesLibres' = filter (/= caseJouee) casesLibres
          hasVainqueur = testerVainqueur $ if couleurCourante == Cyan then donneesCyan' else donneesJaunes'
          vainqueur' = if hasVainqueur then Just couleurCourante else Nothing

plateauGetVainqueurM :: Plateau -> Maybe Couleur
plateauGetVainqueurM (vainqueur, _, _, _, _, _) = vainqueur

plateauGetCyansJaunesLibres :: Plateau -> ([Case], [Case], [Case])
plateauGetCyansJaunesLibres (_, _, _, donneesCyan, donneesJaune, libres) = (cyans, jaunes, libres)
    where cyans = getCases donneesCyan
          jaunes = getCases donneesJaune

-- private
getCouleurSuivante :: Couleur -> Couleur
getCouleurSuivante p = if p == Cyan then Jaune else Cyan

getCases :: DonneesJoueur -> [Case]
getCases (bord1, bord2, composantes) =  casesJouees ++ bord1 ++ bord2
    where casesJouees = foldl ( \acc (l, _, _) -> l ++ acc ) [] composantes

majDonnees :: DonneesJoueur -> Case -> DonneesJoueur
majDonnees (bord1, bord2, composantes) caseJouee = (bord1, bord2, composantes')
    where (connexes, nonConnexes) = partition (isComposanteConnexe caseJouee) composantes
          connexeB1 = isConnexe caseJouee bord1  -- fixme inutile si...
          connexeB2 = isConnexe caseJouee bord2  -- fixme inutile si...
          composanteJouee = ([caseJouee], connexeB1, connexeB2)
          fusionner (accL, accB1, accB2) (l, b1, b2) = (accL ++ l, accB1 || b1, accB2 || b2)
          connexes' = foldl fusionner composanteJouee connexes
          composantes' = connexes' : nonConnexes

isComposanteConnexe :: Case -> Composante -> Bool
isComposanteConnexe c (l, _, _) = isConnexe c l

isConnexe :: Case -> [Case] -> Bool
isConnexe (i, j) = any fConnexe 
    where fConnexe (i', j') = abs (di + dj) < 2 && abs di < 2 && abs dj < 2
              where di = i - i'
                    dj = j - j'

testerVainqueur :: DonneesJoueur -> Bool
testerVainqueur (_, _, composantes) = any f composantes
    where f (_, b1, b2) = b1 && b2
