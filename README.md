# hexhs

 [![Build status](https://gitlab.com/juliendehos/hexhs/badges/master/build.svg)](https://gitlab.com/juliendehos/hexhs/pipelines)

A Haskell implementation of the [HEX board
game](https://en.wikipedia.org/wiki/Hex_%28board_game%29).

![](hexhs.png)

## Run

install ghc, gtk2hs

```
runghc Hexhs.hs
```

## Using Nix

```
cabal2nix . > hexhs.nix
nix-build
./result/bin/hexhs
```

